package lt.sda.battleships;

import java.util.ArrayList;
import java.util.List;

public class ShipFactory {
    public static Ship createTempShip(int x, int y, int size, boolean isVertical) {
        return createShip(x, y, size, isVertical, false);
    }

    public static Ship createShip(int x, int y, int size, boolean isVertical){
        return createShip(x, y, size, isVertical, true);
    }

    private static Ship createShip(int x, int y, int size, boolean isVertical, boolean persist){
        List<Block> blocks = new ArrayList<>();
        if(isVertical){
            int sx = x;
            int sy = y;
            for(int i = 0; i<size; i++){
                blocks.add(new Block(sx, sy, persist));
                sx += 1;
            }
        }
        else {
            int sx = x;
            int sy = y;
            for(int i = 0; i<size; i++){
                blocks.add(new Block(sx, sy, persist));
                sy += 1;
            }
        }

        return new Ship(blocks);
    }
}

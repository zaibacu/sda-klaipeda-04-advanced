package lt.sda.battleships;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Game {
    private Board board;
    private Scanner scanner = new Scanner(System.in);

    public Game(){
        board = new Board(7, 7, 30);
    }

    public void run(){
        board.initialize();
        while(true){
            board.render();
            System.out.println("Your guess: ");
            try {
                String guess = scanner.next(Pattern.compile("[a-z][0-9]"));
                if(board.guess(guess)){
                    System.out.println("HIT!");
                }
                else {
                    System.out.println("Nothing.");
                }
            }
            catch(InputMismatchException ex){
                String incorrectGuess = scanner.next();
                System.err.printf("Incorrect input: '%s'. Please use coordinates like: a5, b4 etc.\n", incorrectGuess);
            }
        }
    }

    public static void main(String[] args){
        new Game().run();
    }
}

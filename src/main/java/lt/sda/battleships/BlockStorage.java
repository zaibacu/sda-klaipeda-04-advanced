package lt.sda.battleships;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

public class BlockStorage {
    private class Point extends AbstractMap.SimpleImmutableEntry<Integer, Integer> {
        public Point(int x, int y) {
            super(x, y);
        }
    }

    private Map<Point, Block> blocks = new HashMap();
    private static BlockStorage instance = null;

    private BlockStorage(){
    }

    public void registerBlock(int x, int y, Block block){
        blocks.put(new Point(x, y), block);
    }

    public Block getBlock(int x, int y){
        return blocks.getOrDefault(new Point(x, y), null);
    }

    public static BlockStorage getInstance(){
        if(instance == null){
            instance = new BlockStorage();
        }

        return instance;
    }
}

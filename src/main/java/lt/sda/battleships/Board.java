package lt.sda.battleships;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {
    List<Ship> ships = new ArrayList<>();
    private int width;
    private int height;
    private int resources;
    private Random random;

    public Board(int width, int height, int resources){
        this.width = width;
        this.height = height;
        this.resources = resources;
        this.random = new Random();
    }

    public Board(int width, int height, int resources, int seed){
        this.width = width;
        this.height = height;
        this.resources = resources;
        this.random = new Random(seed);
    }

    public void render(){
        // O(n^2)
        for(int y = 0; y<this.height; y++){
            for(int x = 0; x<this.width; x++){
                Block tempBlock = BlockStorage.getInstance().getBlock(x, y);
                if(tempBlock != null && tempBlock.isDestroyed()){
                    System.out.printf("x");
                }
                else {
                    System.out.printf(" ");
                }
            }
            System.out.println();
        }
    }

    private boolean isValid(int sx, int sy, int size, boolean isVertical) {
        if(isVertical){
            if(sx + size > width)
                return false;
        }
        else {
            if(sy + size > height)
                return false;
        }

        Ship tempShip = ShipFactory.createTempShip(sx, sy, size, isVertical);
        for(Ship other : ships){
            if(tempShip.collides(other)){
                return false;
            }
        }

        return true;
    }

    private boolean guess(int x, int y){
        for(Ship s : ships){
            if(s.isHit(x, y)){
                return true;
            }
        }

        return false;
    }

    public boolean guess(String g){
        int x = (int)g.charAt(0) - 97;
        int y = Integer.parseInt(g.charAt(1) + "") - 1;
        return guess(x, y);
    }

    public void initialize(){
        while(resources > 0){
            boolean isVertical = random.nextBoolean();
            int size = 2 + random.nextInt(2);
            int rx = random.nextInt(this.width);
            int ry = random.nextInt(this.height);

            /*
            if(resources - size < 0){
                continue;
            }
            */

            if(!isValid(rx, ry, size, isVertical)){
                continue;
            }

            ships.add(ShipFactory.createShip(rx, ry, size, isVertical));
            resources -= size;
        }

    }
}

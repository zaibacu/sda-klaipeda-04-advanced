package lt.sda.battleships;

public class Block {
    private int x;
    private int y;
    private boolean destroyed = false;

    public Block(int x, int y, boolean persist) {
        this.x = x;
        this.y = y;
        if(persist) {
            BlockStorage.getInstance().registerBlock(x, y, this);
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isDestroyed(){
        return destroyed;
    }

    public void setDestroyed(){
        this.destroyed = true;
    }

    public boolean isHit(int x, int y){
        return this.x == x && this.y == y;
    }

    public boolean collides(Block other){
        return this.x == other.getX() && this.y == other.getY();
    }
}

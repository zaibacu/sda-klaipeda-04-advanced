package lt.sda.battleships;

import java.util.List;

public class Ship {
    private final List<Block> blocks;

    protected Ship(List<Block> blocks){
        this.blocks = blocks;
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public boolean collides(Ship other){
        for(Block b1 : blocks){
            for(Block b2 : other.getBlocks()){
                if(b1.collides(b2)){
                    return true;
                }
            }
        }

        return false;
    }

    public boolean isHit(int x, int y){
        for(Block b : blocks){
            if(b.isHit(x, y)) {
                b.setDestroyed();
                if(isSinked()) {
                    System.out.println("the Ship has sunk");
                }
                return true;
            }
        }
        return false;
    }

    public boolean isSinked() {
        long aliveCount = blocks.stream().filter(b -> !b.isDestroyed()).count();
        return aliveCount == 0;
    }
}

package lt.sda.battleships;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class TestRandom {
    @Test
    public void testRandom(){
        Random random = new Random(42);
        assertEquals(0, random.nextInt(10));
    }
}
